package com.example.hibernateDatabaseExample;

import com.example.hibernateDatabaseExample.components.student;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.File;

@SpringBootApplication
public class HibernateDatabaseExampleApplication {

	public static void main(String[] args) {

		//String is used to store the configurationFile name along with extension
		String ConfigurationFile = "hibernate.cfg.xml";
		//below lines are used to fetch the session factory from resources folder and
		// get the session factory object in the class where u want to load it
		ClassLoader classLoaderObj = HibernateDatabaseExampleApplication.class.getClassLoader();
		File f = new File(classLoaderObj.getResource(ConfigurationFile).getFile());
		//the session factory class object from configuration file is fetched
		// and assigned to session factory object
		SessionFactory sessionFactoryObj = new Configuration().configure(f).buildSessionFactory();
		Session sessionObj = sessionFactoryObj.openSession();
		saveRecord(sessionObj);
	}

	private static void saveRecord(Session sessionObj) {
		student s = new student();
		s.setSname("kumar");
		s.setId(2);
		s.setRollno(10);
		s.setStd(17);
		sessionObj.beginTransaction();
		sessionObj.save(s);
		sessionObj.getTransaction().commit();
		System.out.println("Record added");
	}

}
